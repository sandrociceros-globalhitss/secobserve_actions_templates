# Bandit
# ----------------------------------------------------------------
bandit==1.7.8  # https://github.com/PyCQA/bandit
bandit-sarif-formatter==1.1.1  # https://github.com/microsoft/bandit-sarif-formatter

# Checkov
# ----------------------------------------------------------------
checkov==3.2.74  # https://github.com/bridgecrewio/checkov

# Semgrep
# ----------------------------------------------------------------
semgrep==1.69.0  # https://github.com/returntocorp/semgrep

# CryptoLyzer
# ----------------------------------------------------------------
CryptoLyzer==0.12.3  # https://gitlab.com/coroner/cryptolyzer

# Importer
# ----------------------------------------------------------------
requests==2.31.0
